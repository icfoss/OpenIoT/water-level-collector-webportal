from django.contrib import admin
from django.urls import path,include
from . import views


urlpatterns = [
    path('',views.home,name='home'),
    path('dashboard',views.dashboard,name='dashbord'),
    path('well',views.well,name='well'),
    path('well_data',views.well_data,name='well_data'),
]
