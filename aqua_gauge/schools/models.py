from django.db import models
from django.contrib.auth.models import User

class School(models.Model):
    TYPE_CHOICES = (
        ('Public', 'Public'),
        ('Private', 'Private')
    )

    name = models.CharField(max_length=255)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES)
    address = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=20)
    email = models.EmailField()
    website = models.URLField(blank=True)
    country = models.CharField(max_length=255)
    state_province = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    district = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

