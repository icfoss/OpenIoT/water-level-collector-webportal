from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from schools.models import School
from wells.models import Well
from wells.models import WellData
from django .contrib import messages
import datetime
from zoneinfo import ZoneInfo


def get_timedate_initial():
    crnt_time = datetime.datetime.now(tz=ZoneInfo('Asia/Kolkata')) #datetime.timezone.utc
    initial_time = crnt_time.replace(hour=0, minute=0, second=0, microsecond=0)
    previous_date = initial_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    return previous_date

@login_required()
@user_passes_test(lambda u: u.is_staff)
def home(request):
    return render(request,"schools/home.html")

@login_required()
@user_passes_test(lambda u: u.is_staff)
def dashboard(request):
    wells_details = []
    user = request.user
    school = School.objects.get(user=user)
    wells = Well.objects.filter(school_id=school)
    for each in wells:
        # label,depth,last_level,status
        details = []
        details.append(each.label)
        details.append(each.depth)
        details.append(0)
        details.append(0)
        try:
            well_obj = WellData.objects.filter(well_id = each.id).order_by('-datetime')[0]
            details[2] = well_obj.water_level
            WellData.objects.filter(well_id = each.id,datetime__gte = get_timedate_initial()).order_by('-datetime')[0]
            details[3]=1
        except Exception as e:
            print(e)
            details[3]=0
        wells_details.append(details)

    return render(request,"schools/dashboard.html",{
        'wells_count' : len(wells),
        'last_login':user.last_login,
        'well_details' : wells_details
    })


@login_required()
@user_passes_test(lambda u: u.is_staff)
def well(request):
    user = request.user
    school_object = School.objects.get(user=user)
    if(request.method == 'POST'):
        label = request.POST['label']
        depth = request.POST['depth']
        Well.objects.create(
            label=label,
            depth=depth,
            school=school_object 
            )
        messages.warning(request,"Well succesfully added.")
    wells = Well.objects.filter(school = school_object)
    return render(request,"schools/wells.html",{'wells':wells})



@login_required()
@user_passes_test(lambda u: u.is_staff)
def well_data(request):
    user = request.user
    school_object = School.objects.get(user=user)
    wells = Well.objects.filter(school = school_object)

    if(request.method == 'POST'):
        well_id = request.POST['well_id']
        level = request.POST['level']
        datetime = request.POST['datetime']
        well_obj = Well.objects.get(pk=well_id)
        WellData.objects.create(
            well_id=well_obj,
            water_level=level,
            datetime = datetime
            )
        messages.warning(request,"Well data succesfully added.")
    return render(request,"schools/add_well_data.html",{'wells':wells})