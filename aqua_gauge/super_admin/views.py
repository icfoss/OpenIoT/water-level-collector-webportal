from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from schools.models import School
from wells.models import Well
from wells.models import WellData
from django .contrib import messages
from .forms import SchoolForm
from django .contrib.auth.models import User,auth


@login_required()
@user_passes_test(lambda u: u.is_superuser)
def home(request):
    return render(request,"super_admin/home.html")

@login_required()
@user_passes_test(lambda u: u.is_superuser)
def dashboard(request):
    users = len(User.objects.all())
    schools = len(School.objects.all())
    wells = len(Well.objects.all())
    return render(request,"super_admin/dashboard.html",{
        'users':users,
        'schools':schools,
        'wells':wells
    })

@login_required()
@user_passes_test(lambda u: u.is_superuser)
def schools(request):
    school_list = School.objects.all()
    return render(request,"super_admin/schools.html",{'schools':school_list})

@login_required()
@user_passes_test(lambda u: u.is_superuser)
def register(request):
    if request.method == 'POST':
        form = SchoolForm(request.POST)
        if form.is_valid():
            # Create a new user with the data from the form
            user = User.objects.create_user(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password'],
                is_staff = 1
            )
            # Create a new school with the data from the form and the user we just created
            school = School.objects.create(
                name=form.cleaned_data['name'],
                type=form.cleaned_data['type'],
                address=form.cleaned_data['address'],
                phone_number=form.cleaned_data['phone_number'],
                email=form.cleaned_data['email'],
                website=form.cleaned_data['website'],
                country=form.cleaned_data['country'],
                state_province=form.cleaned_data['state_province'],
                city=form.cleaned_data['city'],
                district=form.cleaned_data['district'],
                user=user
            )
            return redirect('schools')
        else:
            messages.warning(request,"Internal server error!")
    form = SchoolForm()
    return render(request,"super_admin/register.html",{'form':form})


@login_required()
@user_passes_test(lambda u: u.is_superuser)
def wells(request):
    if(request.method == 'POST'):
        school_id = request.POST['user']
        school_object = School.objects.get(pk=school_id)
        label = request.POST['label']
        depth = request.POST['depth']
        Well.objects.create(
            label=label,
            depth=depth,
            school=school_object 
            )
        messages.warning(request,"Well succesfully added.")
    wells = Well.objects.all()
    schools = School.objects.all()
    return render(request,"super_admin/wells.html",{
        'wells':wells,
        'schools':schools
        })


@login_required()
@user_passes_test(lambda u: u.is_superuser)
def delete_school(request,id):
    try:
        school = School.objects.get(pk=id)
        school.delete()
        messages.success(request,"School deleted sucessfully")
    except:
        messages.success(request,"Can't delete school")
    return redirect('schools')


@login_required()
@user_passes_test(lambda u: u.is_superuser)
def delete_well(request,id):
    try:
        well = Well.objects.get(pk=id)
        well.delete()
        messages.success(request,"Well deleted sucessfully")
    except:
        messages.success(request,"Can't delete well")
    return redirect('wells')