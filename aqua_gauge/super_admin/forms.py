from django import forms
from django.contrib.auth.models import User
from schools.models import School

class SchoolForm(forms.ModelForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = School
        fields = [
            'name',
            'type',
            'address',
            'phone_number',
            'email',
            'website',
            'country',
            'state_province',
            'city',
            'district',
        ]

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError("This username is already taken")
        return username
