from django.contrib import admin
from django.urls import path,include
from . import views


urlpatterns = [
    path('',views.home,name='home'),
    path('dashboard',views.dashboard,name='dashbord'),
    path('schools',views.schools,name='schools'),
    path('register',views.register,name='register'),
    path('wells',views.wells,name='wells'),
    path('well/delete/<int:id>',views.delete_well,name='delete_well'),
    path('school/delete/<int:id>',views.delete_school,name='delete_school'),
]
