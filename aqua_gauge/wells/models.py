from django.db import models

from schools.models import School

class Well(models.Model):
    label = models.CharField(max_length=100)
    depth = models.DecimalField(max_digits=8, decimal_places=2)
    school = models.ForeignKey(School, on_delete=models.CASCADE)

    def __str__(self):
        return f"Well depth: {self.depth}"


class WellData(models.Model):
    well_id = models.ForeignKey(Well, on_delete=models.CASCADE)
    water_level = models.DecimalField(max_digits=8, decimal_places=2)
    datetime = models.DateTimeField()

    def __str__(self):
        return f"Well ID: {self.well_id}, Water Level: {self.water_level}, Datetime: {self.datetime}"
