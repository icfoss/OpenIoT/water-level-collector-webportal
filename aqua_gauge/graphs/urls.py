from django.contrib import admin
from django.urls import path,include
from . import views


urlpatterns = [
    path('',views.home,name="graph-home"),
    path('wells',views.wells,name="data-wells"),
    path('well/<int:id>/<str:time_range>',views.well_data,name="data-well"),
]
