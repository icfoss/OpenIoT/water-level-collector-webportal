from django.shortcuts import render
from schools.models import School
from wells.models import Well
from wells.models import WellData
from django.http import JsonResponse
import datetime
from zoneinfo import ZoneInfo
from .well_serializer import WellDataSerializer
import json


# get previous datetime with format (yyyy-MM-ddTHH:mm:ss.fZ)
def get_timedate_range(minutes=0,hours=0,days=0,weeks=0,months=0,years=0 ):
    if(years != 0):
        months = years * 12
    if(months != 0 ):
        days = months * 30
    crnt_time = datetime.datetime.now(tz=ZoneInfo('Asia/Kolkata')) #datetime.timezone.utc
    compare_time = crnt_time - datetime.timedelta(minutes = minutes,hours=hours,days=days,weeks=weeks)
    previous_date = compare_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    crnt_date = crnt_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    return [crnt_date,previous_date]


def home(request):
    schools = School.objects.values('id', 'name')
    return render(request,"graphs/home.html",{
        'schools':schools
    })

def wells(request):
    school_id = request.GET['school_id']
    school = School.objects.get(pk=school_id)
    wells = Well.objects.filter(school=school).values('id','label')
    well_level = []
    for each in wells:
        try:
             wellDataObj = WellData.objects.filter(well_id=each['id']).order_by('datetime')[0]
             well_level.append({'label':each['label'],'level':wellDataObj.water_level})
        except Exception as e:
            print(e)
            well_level.append({'label':each['label'],'level':0})

    return JsonResponse({
        'wells' : list(wells),
        'well_level':well_level
    })

def well_data(request,id,time_range):
    well_id = id
    time_ = get_timedate_range(minutes=0,hours=0,days=1,weeks=0,months=0,years=0 )
    if(time_range == 'h-24'):
        time_ = get_timedate_range(minutes=0,hours=0,days=1,weeks=0,months=0,years=0 )
    if(time_range == 'd-1'):
        time_ = get_timedate_range(minutes=0,hours=0,days=1,weeks=0,months=0,years=0 )
    if(time_range == 'w-1'):
        time_ = get_timedate_range(minutes=0,hours=0,days=0,weeks=1,months=0,years=0 )
    if(time_range == 'm-1'):
        time_ = get_timedate_range(minutes=0,hours=0,days=0,weeks=0,months=1,years=0 )
    if(time_range == 'y-1'):
        time_ = get_timedate_range(minutes=0,hours=0,days=1,weeks=0,months=0,years=1 )
    if(time_range == 'f-f'):
        time_ = get_timedate_range(minutes=0,hours=0,days=1,weeks=0,months=0,years=1000 )

    well_data_ = WellData.objects.filter(well_id=well_id,datetime__gte=time_[1], datetime__lte=time_[0]).values('datetime','water_level').order_by('-datetime')
    well_data_serialized = WellDataSerializer(well_data_, many=True).data


    return JsonResponse({
        'well_data' : well_data_serialized
    })
