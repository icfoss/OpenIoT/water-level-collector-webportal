from rest_framework import serializers
from wells.models import WellData

class WellDataSerializer(serializers.ModelSerializer):
    datetime = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    water_level = serializers.FloatField()

    class Meta:
        model = WellData
        fields = ('datetime', 'water_level')