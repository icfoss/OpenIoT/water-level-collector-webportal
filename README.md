# AquaGauge
This project strives to modernize the monitoring of well water levels within the Kattakkada legislative assembly area. The system caters to two user types: administrators and school-level users.

**Key Features:**

- **Admin Capabilities:**
Administrators possess the authority to add schools and wells to the system.
Mapping functionality allows administrators to associate schools with specific wells.

- **School-Level User Access:**
School-level users have dedicated login credentials.
Users at the school level can seamlessly record and input well water level data.

- **Data Visualization:**
The recorded well water level data is made accessible for public visualization.
This transparency ensures community awareness and engagement in monitoring water resources.


By implementing this digital solution, the project contributes to efficient well water management, empowers schools with data-driven insights, and promotes public awareness about water levels in the Kattakkada legislative assembly area.

## Requirements

- [Django](https://pypi.org/project/Django/)
    ```bash
    pip install django
    ```

- [Django REST framework](https://pypi.org/project/djangorestframework/)
    ```bash
    pip install djangorestframework
    ```

- [mysqlclient](https://pypi.org/project/mysqlclient/)
    ```bash
    pip install mysqlclient
    ```


## Installation

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/techworldthink/waterlevelcollector.git
    ```

2. Install project dependencies:
    ```bash
    pip install -r requirements.txt
    ```

3. Configure your MySQL settings in `settings.py`.
    ```python
    # Development
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": "aqua_gauge_db",
        }
    }
    ```
4. Create initial migrations
    ```bash
    python3 manage.py migrate
    ```
5. Apply the migrations and update the database
    ```bash
    python3 manage.py makemigrations
    ```
6. Create a superuser for the admin interface
    ```bash
    python3 manage.py createsuperuser
    ```   
7. Run the Django development server:
    ```bash
    python manage.py runserver
    ```


## Access the project in your web browser:
Open your web browser and navigate to [http://127.0.0.1:8000/](http://127.0.0.1:8000/)


![HOME](Images/home.png)
![VISUALIZATION](Images/visualization.png)
![SCHOOL REGISTER](Images/school_register.png)
![SCHOOL VIEW](Images/school_view.png)
![ADD WELL](Images/add_well.png)
![ADD WELL DATA](Images/add_well_data.png)


## Docker Installation

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/techworldthink/waterlevelcollector.git
    ```
2. Configure your MySQL settings in `settings.py`.
    ```python
    # Production
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'aqua_gauge_db',
            'USER' : 'root',
            'PASSWORD' : 'root',
            'HOST': 'docker-mysql',
        }
    }
    ```
3. Build the Docker container:
    ```bash
    docker build -t your-project-name .
    ```

4. Run the Docker container:
    ```bash
    docker run -p 8000:8000 your-project-name
    ```

5. Access the project in your web browser:
    - Open your web browser and navigate to [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

